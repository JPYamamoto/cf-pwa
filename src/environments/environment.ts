// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyADs3Rfv3Dix2LQaoln2oFPKcG0fsKXf7Y",
    authDomain: "pwa-test-jpy.firebaseapp.com",
    databaseURL: "https://pwa-test-jpy.firebaseio.com",
    projectId: "pwa-test-jpy",
    storageBucket: "pwa-test-jpy.appspot.com",
    messagingSenderId: "450787562085"
  }
};
